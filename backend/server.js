const express = require("express")

const utils = require("./utils")

const app = express()
app.use(express.json())

app.get("/movie/:movie_title", (request, response) => {
  const { movie_title } = request.params

  const connection = utils.openConnection()

  const statement = `
          select * from Movie where
          movie_title = '${movie_title}'
        `
  connection.query(statement, (error, result) => {
    connection.end()

    response.send(utils.createResult(error, result))
  })
})

app.post("/movie", (request, response) => {
  const { movie_title, movie_release_date, movie_time, director_name } =
    request.body

  const connection = utils.openConnection()

  const statement = `
              insert into Movie(movie_title, movie_release_date, movie_time,director_name) values('${movie_title}','${movie_release_date}','${movie_time}','${director_name}')
            `
  connection.query(statement, (error, result) => {
    connection.end()

    response.send(utils.createResult(error, result))
  })
})

app.put("/movie/:movie_title", (request, response) => {
  const { movie_title } = request.params
  const { movie_release_date, movie_time } = request.body

  const connection = utils.openConnection()

  const statement = `
             update Movie set movie_release_date='${movie_release_date}',movie_time='${movie_time}' where movie_title='${movie_title}'
            `
  connection.query(statement, (error, result) => {
    connection.end()

    response.send(utils.createResult(error, result))
  })
})

app.delete("/movie/:movie_title", (request, response) => {
  const { movie_title } = request.params

  const connection = utils.openConnection()

  const statement = `
          delete from Movie where
          movie_title = '${movie_title}'
        `
  connection.query(statement, (error, result) => {
    connection.end()

    response.send(utils.createResult(error, result))
  })
})

app.listen(4000, "0.0.0.0", () => {
  console.log("server started on port 4000")
})
