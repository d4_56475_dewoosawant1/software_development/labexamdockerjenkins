

-- create Movie(movie_id, movie_title, movie_release_date,movie_time,director_name) 
create table Movie(movie_id int primary key auto_increment,
movie_title varchar(20),
movie_release_date varchar(20),
movie_time varchar(20),
director_name varchar(20));

insert into Movie values(1,"RRR","2021-20-20","12 pm","SS Raja mouli");
insert into Movie values(2,"Krish","2021-20-20","12 pm","Rakesh Roshan");